/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See LICENSE.txt for licensing information.
 */
package com.github.kayahr.launch4jtest;

import java.awt.Dimension;
import java.awt.Image;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JLabel;

import com.sun.jna.Native;
import com.sun.jna.NativeLong;
import com.sun.jna.Pointer;
import com.sun.jna.WString;
import com.sun.jna.ptr.PointerByReference;

/**
 * The main class.
 * 
 * @author Klaus Reimer (k@ailis.de)
 */
public class Main
{
    // DO NOT DO THIS, IT'S JUST FOR TESTING PURPOSE AS I'M NOT FREEING THE MEMORY
    // AS REQUESTED BY THE DOCUMENTATION:
    //
    // http://msdn.microsoft.com/en-us/library/dd378419%28VS.85%29.aspx
    //
    // "The caller is responsible for freeing this string with CoTaskMemFree when
    // it is no longer needed"
    public static String getCurrentProcessExplicitAppUserModelID()
    {
        final PointerByReference r = new PointerByReference();

        if (GetCurrentProcessExplicitAppUserModelID(r).longValue() == 0)
        {
            final Pointer p = r.getValue();
            return p.getString(0, true); // here we leak native memory by lazyness
        }      
        return "N/A";
    }

    public static void setCurrentProcessExplicitAppUserModelID(final String appID)
    {
        if (SetCurrentProcessExplicitAppUserModelID(new WString(appID)).longValue() != 0)
            throw new RuntimeException("unable to set current process explicit AppUserModelID to: " + appID);
    }

    private static native NativeLong GetCurrentProcessExplicitAppUserModelID(PointerByReference appID);
    private static native NativeLong SetCurrentProcessExplicitAppUserModelID(WString appID);

    static
    {
        Native.register("shell32");
    }
    
    /**
     * The main method.
     * 
     * @param args
     *            The command line arguments.
     */
    public static void main(String[] args) throws Exception
    {
        setCurrentProcessExplicitAppUserModelID("TestVendor.Launch4jTest");
        System.out.println(getCurrentProcessExplicitAppUserModelID());
        
        List<Image> icons = new ArrayList<Image>();        
        icons.add(ImageIO.read(Main.class.getResource("/icon-16x16x4.png")));
        icons.add(ImageIO.read(Main.class.getResource("/icon-16x16x8.png")));
        icons.add(ImageIO.read(Main.class.getResource("/icon-16x16x32.png")));
        icons.add(ImageIO.read(Main.class.getResource("/icon-24x24x4.png")));
        icons.add(ImageIO.read(Main.class.getResource("/icon-24x24x8.png")));
        icons.add(ImageIO.read(Main.class.getResource("/icon-24x24x32.png")));
        icons.add(ImageIO.read(Main.class.getResource("/icon-32x32x4.png")));
        icons.add(ImageIO.read(Main.class.getResource("/icon-32x32x8.png")));
        icons.add(ImageIO.read(Main.class.getResource("/icon-32x32x32.png")));
        
        JFrame frame = new JFrame("Launch4j Test");
        frame.setName("TestVendor.Launch4jTest");
        frame.setIconImages(icons);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        JLabel label = new JLabel("Nothing to see here. Just a demo.");
        label.setPreferredSize(new Dimension(640, 480));
        label.setVerticalAlignment(JLabel.CENTER);
        label.setHorizontalAlignment(JLabel.CENTER);
        frame.add(label);
        
        frame.pack();
        frame.setVisible(true);
    }
}
